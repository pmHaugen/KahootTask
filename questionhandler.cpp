#include "questionhandler.h"
#include "qdebug.h"

QuestionHandler::QuestionHandler(QObject * parent)
    : QObject{parent}
{
    currentQuestionIndex = 0;
    score = 0;

    loadQuestions("questions.txt");
    timer.start();
}


bool QuestionHandler::giveAnswer(int index)
{
    if(index == tasks[currentQuestionIndex]->correctIndex)
    {
        return true;
    }
    else
        return false;
}

void QuestionHandler::setAnswerIndex(int index)
{
    if(giveAnswer(index))
    {
        calculateScore();
    }
    else
        score -= 10;
    startTimer();
    if(tasks.size() > currentQuestionIndex+1)
        currentQuestionIndex++;
    else
        emit done();

}
void QuestionHandler::calculateScore()
{
    if(getTime() <= 100)
    {
        roundPoints = 100 - (getTime()-50);
        score += roundPoints;

    }
    emit showPointsAnim();

}
QString QuestionHandler::getQuestions()
{
    return tasks[currentQuestionIndex]->question;
}
QString QuestionHandler::getAnsweres(int index)
{
    return tasks[currentQuestionIndex]->answeres[index];
}

void QuestionHandler::startTimer()
{
    timer.restart();
}

void QuestionHandler::generateQuestions()
{

    tasks.push_back(new questions);
    tasks[0]->question = "How many fingers does a human have?";
    tasks[0]->answeres[0] = "1"; tasks[0]->answeres[1] = "5"; tasks[0]->answeres[2] = "3"; tasks[0]->answeres[3] = "4";
    tasks[0]->correctIndex = 1;

    tasks.push_back(new questions);
    tasks[1]->question = "But how many do they really have???";
    tasks[1]->answeres[0] = "5"; tasks[1]->answeres[1] = "5"; tasks[1]->answeres[2] = "10"; tasks[1]->answeres[3] = "5";
    tasks[1]->correctIndex = 2;

    tasks.push_back(new questions);
    tasks[2]->question = "What is the biggest mamal of these options?";
    tasks[2]->answeres[0] = "African bush elephant"; tasks[2]->answeres[1] = "Asian elephant"; tasks[2]->answeres[2] = "African forest elephant"; tasks[2]->answeres[3] = "White Rhinoceros";
    tasks[2]->correctIndex = 0;

    tasks.push_back(new questions);
    tasks[3]->question = "What was the largest dinosaur of these options?";
    tasks[3]->answeres[0] = "Maraapunisaurus"; tasks[3]->answeres[1] = "Tyrannosaurus Rex"; tasks[3]->answeres[2] = "Argentinosaurus"; tasks[3]->answeres[3] = "Supersaurus";
    tasks[3]->correctIndex = 3;

    tasks.push_back(new questions);
    tasks[4]->question = "What year was the first Digimon World game released?";
    tasks[4]->answeres[0] = "1980"; tasks[4]->answeres[1] = "2005"; tasks[4]->answeres[2] = "1990"; tasks[4]->answeres[3] = "1999";
    tasks[4]->correctIndex = 3;

}

void QuestionHandler::saveQuestions(QString fileName)
{
    QFile file(fileName);
    if(file.open(QFile::WriteOnly | QFile::Text))
    {
        QTextStream out(&file);
        for (size_t i = 0; i < tasks.size(); i++)
        {
            out << tasks[i]->question << "#" << tasks[i]->answeres[0] << "#" << tasks[i]->answeres[1]
                << "#" << tasks[i]->answeres[2] << "#" << tasks[i]->answeres[3] << "#"
                << tasks[i]->correctIndex << "#" << "\n";
        }
        file.close();
    }
    else
    {
        qDebug() << "Couldn't open the file for writing\n";
    }
}

void QuestionHandler::loadQuestions(QString fileName)
{
    QFile file(fileName);

    if(file.open(QFile::ReadOnly | QFile::Text))
    {
        QTextStream in(&file);
        //std::cout << in.readAll().toStdString();

        while(!in.atEnd())
        {
            QList<QString> loadData;
            QString line = in.readLine();
            QString word;
            int wordLocation = 0;
            for(int i = 0; i < line.length(); i++)
            {
                word += line.at(i);
                wordLocation++;
                if(word.at(wordLocation-1) == '#')
                {
                    word.chop(1);
                    loadData.push_back(word);
                    wordLocation = 0;
                    word.clear();
                }
            }
            questions *questionPtr = new questions;
            tasks.push_back(questionPtr);
            questionPtr->question = loadData[0];
            questionPtr->answeres[0] = loadData[1];
            questionPtr->answeres[1] = loadData[2];
            questionPtr->answeres[2] = loadData[3];
            questionPtr->answeres[3] = loadData[4];
            questionPtr->correctIndex = loadData[5].toInt();


            //qDebug() << questionPtr->question << loadData[0];
        }
        file.close();
    }
    else
    {
        qDebug() << "Couldn't open the file for reading\n";
    }
}
void QuestionHandler::addQuestion(QList<QString> questionData)
{
    questions * newQuestion = new questions;
    tasks.push_back(newQuestion);
    newQuestion->question = questionData[0];
    qDebug() << questionData[0];
    newQuestion->answeres[0] = questionData[1];
    newQuestion->answeres[1] = questionData[2];
    newQuestion->answeres[2] = questionData[3];
    newQuestion->answeres[3] = questionData[4];
    newQuestion->correctIndex = questionData[5].toInt()-1;

    saveQuestions("questions.txt");
}

float QuestionHandler::getTime()
{
    return timer.elapsed() / 100;

}

