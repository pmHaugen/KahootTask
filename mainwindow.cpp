#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QIntValidator>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFile stylefile(":/new/stylesheet/stylesheet.css");
    stylefile.open(QFile::ReadOnly);
    style = QLatin1String(stylefile.readAll());
    qApp->setStyleSheet(style);

    qHandler = new QuestionHandler;

    connect(ui->pushButton_answerOne, &QPushButton::clicked, this, [=]()
    {answerOptionClicked(0);});

    connect(ui->pushButton_answerTwo, &QPushButton::clicked, this, [=]()
    {answerOptionClicked(1);});

    connect(ui->pushButton_answerThree, &QPushButton::clicked, this, [=]()
    {answerOptionClicked(2);});

    connect(ui->pushButton_answerFour, &QPushButton::clicked, this, [=]()
    {answerOptionClicked(3);});

    connect(qHandler, &QuestionHandler::done, this, &MainWindow::whenDone);
    connect(ui->returntomain, &QPushButton::clicked, ui->stackedWidget, [=](){ui->stackedWidget->setCurrentIndex(0);});

    connect(qHandler, &QuestionHandler::showPointsAnim, this, &MainWindow::showPointsAnim);


    connect(ui->addQuestion, &QLineEdit::textChanged, this, [=]()
    {QString arg1 = ui->addQuestion->text();textEditValidate(arg1, 0);});

    connect(ui->addOptionOne, &QLineEdit::textChanged, this, [=]()
    {QString arg1 = ui->addOptionOne->text();textEditValidate(arg1, 1);});

    connect(ui->addOptionTwo, &QLineEdit::textChanged, this, [=]()
    {QString arg1 = ui->addOptionTwo->text();textEditValidate(arg1, 2);});

    connect(ui->addOptionThree, &QLineEdit::textChanged, this, [=]()
    {QString arg1 = ui->addOptionThree->text();textEditValidate(arg1, 3);});

    connect(ui->addOptionFour, &QLineEdit::textChanged, this, [=]()
    {QString arg1 = ui->addOptionFour->text();textEditValidate(arg1, 4);});

    connect(ui->addOptionIndex, &QLineEdit::textChanged, this, [=]()
    {QString arg1 = ui->addOptionIndex->text();textEditValidate(arg1, 5);});


    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MainWindow::update);
    timer->start(50);

    ui->timeProgress->setMaximum(100);
    ui->timeProgress->setValue(100);

    ui->addOptionIndex->setValidator(new QIntValidator(1, 4, this));

    ui->stackedWidget->setCurrentIndex(0);
    ui->scoreValue->setText("0");

    int width = this->size().width();
    int height = this->size().height();
    animation = new QPropertyAnimation(ui->showPoints, "geometry");
    animation->setDuration(700);
    animation->setStartValue(QRect(width/2,height, 60,50));
    animation->setEndValue(QRect(width/2, height/2, 60,50));
}


MainWindow::~MainWindow()
{
    delete ui;
    delete qHandler;
    delete timer;
}

void MainWindow::whenDone()
{
    ui->finalScoreValue->setText(QString::number(qHandler->score));
    ui->UserNameFinal->setText(userName);
    ui->stackedWidget->setCurrentIndex(2);
    qHandler->score = 0;
    qHandler->currentQuestionIndex = 0;
}

void MainWindow::on_startGameButton_clicked()
{
    userName = ui->nameField->text();
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex() + 1);
    ui->userName->setText(userName);
    ui->questionBox->setText(qHandler->getQuestions());
    ui->pushButton_answerOne->setText(qHandler->getAnsweres(0));
    ui->pushButton_answerTwo->setText(qHandler->getAnsweres(1));
    ui->pushButton_answerThree->setText(qHandler->getAnsweres(2));
    ui->pushButton_answerFour->setText(qHandler->getAnsweres(3));
    ui->scoreValue->setText(QString::number(qHandler->score));
    ui->showPoints->setEnabled(false);
    ui->showPoints->setText(" ");
    ui->confirmQuestions->setEnabled(false);
    update();
    qHandler->timer.start();
}
void MainWindow::update()
{
    if(qHandler->getTime() < 50)
    {
        ui->timeProgress->setValue(100);
        ui->countdownLabel->setText(QString::number(5 - qHandler->getTime()/10));
        enableButtons(false);
    }
    else if (!ui->pushButton_answerOne->isEnabled())
    {
        enableButtons(true);
    }
    else
        ui->timeProgress->setValue(150 - qHandler->getTime());

    if(qHandler->getTime() > 20 && qHandler->getTime() < 30)
        ui->showPoints->setText("");
    ui->startGameButton->setEnabled(!qHandler->tasks.empty());
}
void MainWindow::answerOptionClicked(int index)
{
    qHandler->setAnswerIndex(index);
    ui->questionBox->setText(qHandler->getQuestions());
    ui->pushButton_answerOne->setText(qHandler->getAnsweres(0));
    ui->pushButton_answerTwo->setText(qHandler->getAnsweres(1));
    ui->pushButton_answerThree->setText(qHandler->getAnsweres(2));
    ui->pushButton_answerFour->setText(qHandler->getAnsweres(3));
    ui->scoreValue->setText(QString::number(qHandler->score));
}

void MainWindow::enableButtons(bool state)
{
    ui->pushButton_answerOne->setEnabled(state);
    ui->pushButton_answerTwo->setEnabled(state);
    ui->pushButton_answerThree->setEnabled(state);
    ui->pushButton_answerFour->setEnabled(state);
    ui->countdownLabel->setEnabled(!state);
    ui->secondsLabel->setEnabled(!state);
}
void MainWindow::showPointsAnim()
{
    int width = this->size().width();
    int height = this->size().height();
    ui->showPoints->setText("+" + QString::number(qHandler->roundPoints));
    ui->showPoints->setEnabled(true);
    ui->showPoints->raise();
    QRect end = ui->showPoints->geometry();
    animation->setStartValue(QRect(width/2-75,height, 150,50));
    animation->setEndValue(end);
    animation->start();
}

void MainWindow::on_addQuestionButton_clicked()
{
    on_cancelAddQuestion_clicked();
    taskList.clear();
    for(size_t i = 0; i < questionFieldsFilled.size(); i++)
    {
        questionFieldsFilled[i] = false;
    }
    ui->stackedWidget->setCurrentIndex(3);
}

void MainWindow::on_confirmQuestions_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    qHandler->addQuestion(taskList);
}

void MainWindow::on_cancelAddQuestion_clicked()
{
    ui->questionList->clear();
    ui->stackedWidget->setCurrentIndex(0);
    ui->addQuestion->setText("");
    ui->addOptionOne->setText("");
    ui->addOptionTwo->setText("");
    ui->addOptionThree->setText("");
    ui->addOptionFour->setText("");
    ui->addOptionIndex->setText("");
}

void MainWindow::textEditValidate(const QString &arg1, int index)
{
    if(taskList.length() > index)
    {
        taskList[index] = arg1;
        questionFieldsFilled[index] = true;
        //qDebug() << "adding characters" << taskList[index];
    }
    else if(arg1.length()>0)
    {
        while(taskList.length() < 6)
            {taskList.push_back(nullptr);}
        taskList[index] = arg1;
        questionFieldsFilled[index] = true;
        //qDebug() << "adding space" << taskList[index];
    }
    if(arg1.length()<1)
    {
        questionFieldsFilled[index] = false;
    }
    if(index == 5)
    {
        QIntValidator v(1,4,this);
        QString unvalidatedValue = arg1;
        int pos = 1;
        if(v.validate(unvalidatedValue, pos) != QIntValidator::Acceptable)
        {
            ui->addOptionIndex->clear();
        }
    }
    checkIfCanConfirm();
}


void MainWindow::checkIfCanConfirm()
{
    bool isTrue = true;
    for(size_t i = 0; i < questionFieldsFilled.size(); i++)
    {
        if(questionFieldsFilled[i] == false)
        {
            isTrue = false;
        }
    }
    if(isTrue == true)
    {
        ui->confirmQuestions->setEnabled(true);
    }
    else
        ui->confirmQuestions->setEnabled(false);
}

void MainWindow::on_nameField_returnPressed()
{
    on_startGameButton_clicked();
}

void MainWindow::on_editQuestions_clicked()
{
    ui->questionList->clear();
    for(size_t i = 0; i < qHandler->tasks.size(); i++)
    {
        ui->questionList->addItem(qHandler->tasks[i]->question);
    }
    ui->stackedWidget->setCurrentIndex(4);
}


void MainWindow::on_deleteQuestion_clicked()
{
    qHandler->tasks.erase(qHandler->tasks.begin()+ui->questionList->currentRow());
    delete ui->questionList->takeItem(ui->questionList->currentRow());
    qHandler->saveQuestions("questions.txt");
}

void MainWindow::on_cancelEdit_clicked()
{
    ui->questionList->clear();
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_questionEditor_clicked()
{

    for(size_t i = 0; i < questionFieldsFilled.size(); i++)
    {
        questionFieldsFilled[i] = true;
    }
    int index = ui->questionList->currentRow();
    ui->questionList->clear();
    ui->addQuestion->setText(qHandler->tasks[index]->question);
    ui->addOptionOne->setText(qHandler->tasks[index]->answeres[0]);
    ui->addOptionTwo->setText(qHandler->tasks[index]->answeres[1]);
    ui->addOptionThree->setText(qHandler->tasks[index]->answeres[2]);
    ui->addOptionFour->setText(qHandler->tasks[index]->answeres[3]);
    ui->addOptionIndex->setText(QString::number(qHandler->tasks[index]->correctIndex+1));
    qHandler->tasks.erase(qHandler->tasks.begin()+index);
    ui->stackedWidget->setCurrentIndex(3);
}

void MainWindow::on_generateQuestions_clicked()
{
    qHandler->tasks.clear();
    qHandler->generateQuestions();
    qHandler->saveQuestions("questions.txt");
}
