# PaalMaBruuHaoot!
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

This is a kahoot mock up designed to have the basic functions of kahoot including the ability show the user questions, possible answers, 
having a username, showing amount of points gathered, use some form of animation and use custom styling of font and layout.

Ekstra features such as saving, loading, editing deleting and adding questions has also been made for learning reasons.

![alt picture](kahoot.png)
## Requirments
This project depends on
* [QT 6.3.1 MinGW 64bit](https://https://www.qt.io/)



## Install
This is a QT project that can be run by opening the project file with QT and configure it to use version 6.3.1.


## Usage
When running the program for the first time, you need to press generate standard questions button to play the game or add your own questions to start.



### Contributing
*Paal Marius Haugen


## License
--

