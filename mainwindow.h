#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <questionhandler.h>
#include <QPropertyAnimation>
#include <QTimer.h>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QString style;

private slots:
    void on_startGameButton_clicked();
    void whenDone();
    void showPointsAnim();

    void on_addQuestionButton_clicked();
    void on_confirmQuestions_clicked();
    void on_cancelAddQuestion_clicked();

    void on_nameField_returnPressed();

    void on_editQuestions_clicked();

    void on_deleteQuestion_clicked();

    void on_cancelEdit_clicked();


    void on_questionEditor_clicked();

    void on_generateQuestions_clicked();

private:
    Ui::MainWindow *ui;
    QuestionHandler * qHandler;
    QString userName;
    QTimer *timer;
    QPropertyAnimation * animation;
    void update();
    void returnToStart();
    void answerOptionClicked(int);
    void enableButtons(bool);
    void checkIfCanConfirm();
    void textEditValidate(const QString &, int);
    QList<QString> taskList;
    std::array<bool, 6> questionFieldsFilled;


};
#endif // MAINWINDOW_H
