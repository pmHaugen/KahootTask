#ifndef QUESTIONHANDLER_H
#define QUESTIONHANDLER_H
#include <iostream>
#include <array>
#include <vector>
#include <QObject>
#include <QElapsedTimer>
#include <QFile>
#include <QTextStream>

struct questions
{
    QString question;
    std::array<QString, 4> answeres;
    int correctIndex;

};

class QuestionHandler : public QObject
{
    Q_OBJECT
public:
    explicit QuestionHandler(QObject * parent = nullptr);
    bool giveAnswer(int);
    void calculateScore();
    size_t currentQuestionIndex;
    QString getQuestions();
    QString getAnsweres(int index);
    float score;
    std::vector<questions*> tasks;
    void startTimer();
    QElapsedTimer timer;
    int time;
    int roundPoints;
    void generateQuestions();
    void addQuestion(QList<QString>);
    void saveQuestions(QString fileName);
    void loadQuestions(QString fileName);
signals:
    void done();
    void timeSignal();
    void showPointsAnim();


public slots:
    void setAnswerIndex(int index);
    float getTime();

};

#endif // QUESTIONHANDLER_H
